# Лабораторная работа #2

**Дисциплина:** "Функциональное программирование"

**Выполнил:** Носов Михаил, P34102

**Название:** "Реализация структуры данных"

**Цель работы:** 
 Освоиться с построением пользовательских типов данных, полиморфизмом, рекурсивными алгоритмами и средствами тестирования (unit testing, property-based testing).

**Требования**
1. Функции:
- добавление и удаление элементов;
- фильтрация;
- отображение (map);
- свертки (левая и правая);
- структура должна быть моноидом.
2. Структуры данных должны быть неизменяемыми.
3. Библиотека должна быть протестирована в рамках unit testing.
4. Библиотека должна быть протестирована в рамках property-based тестирования (как минимум 3 свойства, включая свойства монойда).
5. Структура должна быть полиморфной.
6. Требуется использовать идиоматичный для технологии стиль программирования.

## Вариант - prefix tree

Описание структуры
```
(deftype Trie [trie lexicon-set]

  clojure.lang.ILookup
  (valAt [self prefix-string] (get lexicon-set prefix-string))
  (valAt [self prefix-string default] (get lexicon-set prefix-string default))

  clojure.lang.IPersistentCollection
  (seq [self]     (seq lexicon-set))
  (cons [self o]  (Trie. (conj-trie trie o) (conj lexicon-set o)))
  (empty [self]   (Trie. (make-empty-trie) #{}))
  (equiv [self o] (and (instance? Trie o) (= trie (.trie o))))

  clojure.lang.IFn
  (invoke [self prefix-string]
    (let [completions (apply-trie trie prefix-string)]
      (if (seq completions) completions nil)))
```

Вспомогательные методы
```
(defn make-empty-trie
  []
  {:value ""})

(defn trie->seq
  [{:keys [value children key] :as trie}]
  (loop [stack [trie]
         words []]
    (if (not (seq stack))
      (seq words)
      (let [{:keys [value children key]} (peek stack)]
        (recur (into (pop stack) (vals children))
               (if key (conj words value) words))))))

(defn apply-trie
  [{:keys [value key children] :as trie} [c & cs :as string]]
  (cond
    (not (seq string))           (trie->seq trie)
    (not (get children (str c))) []
    :else                        (recur (get children (str c)) cs)))

(defn conj-trie
  [{:keys [value children] :as trie} [c & cs :as string]]
  (let [ch (str c)]
    (cond
      (and (not (get children ch)) (not (seq cs)))
      (assoc-in trie [:children (str c)] {:value (str value c) :key (gensym)})

      (and (not (get children ch)) (seq cs))
      (assoc-in trie [:children ch] (conj-trie {:value (str value c)} cs))

      (and (get children ch) (not (seq cs)))
      (assoc-in trie [:children ch :key] (gensym))

      (and (get children ch) (seq cs))
      (update-in trie [:children ch] conj-trie cs))))

(defn into-trie
  [trie coll]
  (reduce conj-trie trie coll))
```
## Property-based testing

```
(def words-gen
  (->> gen/string-alpha-numeric
       (gen/such-that not-empty)
       gen/set
       (gen/such-that seq)))

(def insert-trie-property
  (prop/for-all [words words-gen]
                (->> words trie seq set (= words))))

(def autocomplete-property
  (prop/for-all [words words-gen]
                (let [prefix-string (first (shuffle words))]
                  (= (set ((trie words) prefix-string))
                     (set (filter (partial re-matches
                                           (re-pattern (str "^" prefix-string ".*")))
                                  words))))))

(def contains-property
  (prop/for-all [words words-gen]
    (let [test-trie (trie words)
          test-word (first (shuffle words))]
      (= (get test-trie test-word) test-word))))

(defspec should-insert-all-in-trie 100 insert-trie-property)
(defspec should-autocomplete 100 autocomplete-property)
(defspec should-contain 100 contains-property)
```
Вывод:
```
lein test lab2.core-test
{:result true, :num-tests 100, :seed 1678185300319, :test-var "should-insert-all-in-trie"}
{:result true, :num-tests 100, :seed 1678185300676, :test-var "should-contain"}
{:result true, :num-tests 100, :seed 1678185300944, :test-var "should-autocomplete"}

Ran 3 tests containing 3 assertions.
0 failures, 0 errors.
```
## Заключение

В ходе выполнения данной работы я изучил большое количество литературы по теме, выяснил, что строгой документации для создания собственных коллекций нет. 
