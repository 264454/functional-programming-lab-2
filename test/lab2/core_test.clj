(ns lab2.core-test
  (:require [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [lab2.trie :refer :all]))

(def words-gen
  (->> gen/string-alpha-numeric
       (gen/such-that not-empty)
       gen/set
       (gen/such-that seq)))

(def insert-trie-property
  (prop/for-all [words words-gen]
                (->> words trie seq set (= words))))

(def autocomplete-property
  (prop/for-all [words words-gen]
                (let [prefix-string (first (shuffle words))]
                  (= (set ((trie words) prefix-string))
                     (set (filter (partial re-matches
                                           (re-pattern (str "^" prefix-string ".*")))
                                  words))))))

(def contains-property
  (prop/for-all [words words-gen]
    (let [test-trie (trie words)
          test-word (first (shuffle words))]
      (= (get test-trie test-word) test-word))))

(defspec should-insert-all-in-trie 100 insert-trie-property)
(defspec should-autocomplete 100 autocomplete-property)
(defspec should-contain 100 contains-property)