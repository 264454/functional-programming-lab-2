(ns lab2.trie
  (:require [clojure.string :as s])
  (:import (clojure.lang IPersistentCollection Associative Util ILookup IMapEntry Seqable IPersistentCollection IMeta MapEquivalence IPersistentVector)
           (java.util Map)))


(defn make-empty-trie
  []
  {:value ""})

(defn trie->seq
  [{:keys [value children key] :as trie}]
  (loop [stack [trie]
         words []]
    (if (not (seq stack))
      (seq words)
      (let [{:keys [value children key]} (peek stack)]
        (recur (into (pop stack) (vals children))
               (if key (conj words value) words))))))

(defn apply-trie
  [{:keys [value key children] :as trie} [c & cs :as string]]
  (cond
    (not (seq string))           (trie->seq trie)
    (not (get children (str c))) []
    :else                        (recur (get children (str c)) cs)))

(defn conj-trie
  [{:keys [value children] :as trie} [c & cs :as string]]
  (let [ch (str c)]
    (cond
      (and (not (get children ch)) (not (seq cs)))
      (assoc-in trie [:children (str c)] {:value (str value c) :key (gensym)})

      (and (not (get children ch)) (seq cs))
      (assoc-in trie [:children ch] (conj-trie {:value (str value c)} cs))

      (and (get children ch) (not (seq cs)))
      (assoc-in trie [:children ch :key] (gensym))

      (and (get children ch) (seq cs))
      (update-in trie [:children ch] conj-trie cs))))

(defn into-trie
  [trie coll]
  (reduce conj-trie trie coll))

(deftype Trie [trie lexicon-set]

  clojure.lang.ILookup
  (valAt [self prefix-string] (get lexicon-set prefix-string))
  (valAt [self prefix-string default] (get lexicon-set prefix-string default))

  clojure.lang.IPersistentCollection
  (seq [self]     (seq lexicon-set))
  (cons [self o]  (Trie. (conj-trie trie o) (conj lexicon-set o)))
  (empty [self]   (Trie. (make-empty-trie) #{}))
  (equiv [self o] (and (instance? Trie o) (= trie (.trie o))))

  clojure.lang.IFn
  (invoke [self prefix-string]
    (let [completions (apply-trie trie prefix-string)]
      (if (seq completions) completions nil)))

  Object
  (toString [self] (str trie)))

(defn trie
  ([]     (Trie. (make-empty-trie) #{}))
  ([coll] (into (trie) (set  coll))))
